<?php

namespace App\Services\API\v2;

use App\Models\ShortUrl;
use Illuminate\Support\Str;

class ShortUrlService
{
    public function store(array $data, int $userId): ShortUrl
    {
        $shortenedUrl = $this->createShortUrl();

        $shortUrl = ShortUrl::where('original_url', $data['original_url'])->where('user_id', $userId)->first();

        $data = array_merge([
            'user_id'   =>  $userId,
            'short_url' =>  $shortenedUrl,
            'hits'      =>  $shortUrl->hits ?? 0
        ], $data);

        
        if(!isset($shortUrl)) {
            $shortUrl = ShortUrl::create($data);
        }
        
        return $shortUrl;
    }

    protected function createShortUrl()
    {
        return  url(Str::random(8));
    }
}