<?php

namespace App\Services\API\v1;

use App\Models\ShortUrl;
use Illuminate\Support\Str;

class ShortUrlService
{
    public function store(array $data, int $userId): ShortUrl
    {
        $shortenedUrl = $this->createShortUrl();

        $data = array_merge([
            'user_id'   =>  $userId,
            'short_url' =>  $shortenedUrl
        ], $data);

        $shortUrl = ShortUrl::where('original_url', $data['original_url'])->where('user_id', $userId)->first();
        
        if(!isset($shortUrl)) {
            $shortUrl = ShortUrl::create($data);
        }

        return $shortUrl;
    }

    protected function createShortUrl()
    {
        return  url(Str::random(8));
    }
}