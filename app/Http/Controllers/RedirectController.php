<?php

namespace App\Http\Controllers;

use App\Models\ShortUrl;

class RedirectController extends Controller
{
    public function redirectTo()
    {
        $url = request()->url();

        $shortUrl = ShortUrl::where('short_url', $url)->first();
        
        if(!isset($shortUrl)) {
            abort(404);
        }
       
        $shortUrl->increment('hits');
        
        return redirect()->away($shortUrl->original_url);
    }
}
