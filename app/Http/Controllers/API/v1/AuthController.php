<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\API\v1\AuthLoginRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(AuthLoginRequest $request) {
        $validatedFields = $request->validated();
     
        $user = User::where('email', $validatedFields['email'])->first();
     
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
     
        $token = $user->createToken('Personal Access Token')->plainTextToken;

        $response = [
            'token'        => $token,
            'token_type'   => 'Bearer',
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function logout(Request $request) 
    {
        $request->user()->tokens()->delete();

        $response = [
            'messsage'  => 'Logged out',
        ];

        return response()->json($response, Response::HTTP_OK);
    }
}
