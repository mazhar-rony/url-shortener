<?php

namespace App\Http\Controllers\API\v2;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\API\v2\RegisteredUserRequest;

class RegisteredUserController extends Controller
{
    public function register(RegisteredUserRequest $request) {
        $validatedFields = $request->validated();

        $validatedFields['password'] = Hash::make($validatedFields['password']);

        User::create($validatedFields);

        $response = [
            'message'  => 'User registration successfull',
        ];

        return response()->json($response, Response::HTTP_CREATED);
    }
}
