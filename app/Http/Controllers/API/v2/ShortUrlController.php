<?php

namespace App\Http\Controllers\API\v2;

use App\Models\ShortUrl;
use App\Http\Controllers\Controller;
use App\Services\API\v2\ShortUrlService;
use App\Http\Resources\v2\ShortUrlResource;
use App\Http\Requests\API\v2\ShortUrlRequest;
use App\Http\Resources\v2\ShortUrlCollection;
use Symfony\Component\HttpFoundation\Response;

class ShortUrlController extends Controller
{
    public function index()
    {
        $shortUrls = ShortUrl::where('user_id', request()->user()->id)->paginate(10);
        
        return new ShortUrlCollection($shortUrls);
    }

    public function store(ShortUrlRequest $request, ShortUrlService $shortUrlService) 
    {   
        $shortUrl = $shortUrlService->store($request->validated(), $request->user()->id);
        
        return response()->json(new ShortUrlResource($shortUrl), Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);   
    }
}
