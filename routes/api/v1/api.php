<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\AuthController;
use App\Http\Controllers\API\v1\ShortUrlController;
use App\Http\Controllers\API\v1\RegisteredUserController;


    Route::post('register', [RegisteredUserController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function() {
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('url', [ShortUrlController::class, 'store']);
        Route::get('get-urls', [ShortUrlController::class, 'index']);
    });

